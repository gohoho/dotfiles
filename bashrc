# ~/.bashrc
#
PATH="`ruby -e 'puts Gem.user_dir'`/bin:$PATH"
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#PS1='[\u@\h \W]\$ '
set -o vi
# autocd when entering the path
shopt -s autocd
# correct small typing mistakes of cd
shopt -s cdspell
# append to history, don't overwrite it
shopt -s histappend

bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

#чтобы одинаковые команды не сохранялись в истории, добавьте строчку:
histcontrol=ignoredups
#Не добавлять запись в историю команд, если команда начинается с пробела:
HISTCONTROL=ignorespace
#стираются все повторения, в том числе идущие не подряд, но в разной последовательности. (например, после cd..ls..cd..ls останется cd и ls)
HISTCONTROL=erasedups
PROMPT_COMMAND='history -a' 

alias ls='ls --color=auto'
alias ll='ls -l --color=auto'
alias chroxy="chromium --proxy-server='127.0.0.1:3128'"
alias xrandrtwo='xrandr --output HDMI1 --primary --output VGA1 --right-of HDMI1'

#git aliases
alias gits='git status '
alias gita='git add '
alias gitb='git branch '
alias gitc='git commit'
alias gitd='git diff'
alias gitl='git lg'
alias go='git checkout '
alias gk='gitk --all&'

alias got='git '
alias get='git '

alias cal='cal -m'
#svn aliases
alias svn_delete_excl_mark="svn st | grep ! | awk '{print $2}' | xargs svn delete --force"
export EDITOR=gvim
export TERMINAL='urxvtc -e'
# virtualbox env
VBOX_USB=usbfs

alias envoy_all='envoy -t ssh-agent -a id_rsa_gitlab id_rsa_bitbucket id_rsa_github; source <(envoy -p)'
alias envoy_gl='envoy -t ssh-agent -a id_rsa_gitlab; source <(envoy -p)'
alias envoy_gh='envoy -t ssh-agent -a id_rsa_github; source <(envoy -p)'
alias envoy_bb='envoy -t ssh-agent -a id_rsa_bitbucket; source <(envoy -p)'

export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Devel
source /usr/bin/virtualenvwrapper.sh

function swap()         
{
    local TMPFILE=tmp.$$
    mv "$1" $TMPFILE
    mv "$2" "$1"
    mv $TMPFILE "$2"
}

function arduino_mod()
{
    sudo chown root:lock /run/lock
    sudo chmod g+w /run/lock
}
function proxy(){
#    echo -n "username:"
#    read -e username
#    echo -n "password:"
#    read -es password
#    export http_proxy="http://$username:$password@proxyserver:8080/"
    export http_proxy="http://127.0.0.1:3128/"
    export https_proxy=$http_proxy
    export ftp_proxy=$http_proxy
    export rsync_proxy=$http_proxy
    export all_proxy=$http_proxy
    export no_proxy="localhost,127.0.0.1,localaddress,.localdomain.com"
    echo -e "\nProxy environment variable set."
}

function proxyoff(){
    unset HTTP_PROXY
    unset http_proxy
    unset HTTPS_PROXY
    unset https_proxy
    unset FTP_PROXY
    unset ftp_proxy
    unset RSYNC_PROXY
    unset rsync_proxy
    echo -e "\nProxy environment variable removed."
}
