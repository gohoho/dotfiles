from libqtile.config import Key, Click, Drag, Screen, Group, Match
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook

from socket import gethostname

hostname = gethostname()
home_hostname = 'arch-home'
work_hostname = 'n0213'

mod = "mod4"
alt = "mod1"
ctrl = "control"
shift = "shift"

keys = [
    # Common options###############################
    # Switch between layouts
    Key([mod], "space", lazy.group.next_window()),
    Key([mod, shift], "space", lazy.group.prev_window()),

    Key([mod], "g", lazy.to_screen(0)),
    Key([mod], "h", lazy.to_screen(1)),

    Key([mod], "s", lazy.switch_groups()),
    Key([mod], "h", lazy.nextGroup()),
    Key([mod], "l", lazy.prevGroup()),
 
    Key([mod], "Tab", lazy.nextlayout()),
    Key([mod, shift], "Tab", lazy.prevlayout()),

    Key([mod], "o", lazy.layout.section_down()),
    Key([mod], "p", lazy.layout.section_up()),
    Key([mod, shift], "o", lazy.layout.increase_nmaster()),
    Key([mod, shift], "p", lazy.layout.decrease_nmaster()),

    # restart qtile
    Key([mod, shift, ctrl], "r", lazy.restart()),
    # restart X
    Key([mod, shift, ctrl], "q", lazy.shutdown()),
    # shutdown
    Key([mod, alt, ctrl], "q", lazy.spawn("shutdown -now")),
    Key([mod, shift], "w", lazy.window.kill()),

    # Changing volume the old fashioned way.
    Key([mod], "minus", lazy.spawn("amixer -c 0 -q set Master 2- unmute")),
    Key([mod], "equal", lazy.spawn("amixer -c 0 -q set Master 2+ unmute")),
    Key([mod, "shift"], "equal", lazy.spawn("amixer sset Master toggle")),
    
    # Switch between windows in current stack pane
    Key([mod], "i", lazy.layout.down()),
    Key([mod], "u", lazy.layout.up()),
    
    # Toggle vertical split on the current stack.
    Key([mod], "comma", lazy.layout.toggle_split()),

    # Move windows up or down in current stack
    Key([mod, shift], "i", lazy.layout.shuffle_down()),
    Key([mod, shift], "u", lazy.layout.shuffle_up()),
    # Stack ########################################## 
    
    Key([mod], "period", lazy.layout.add()),
    Key([mod, shift], "period", lazy.layout.delete()),
    
    # MonadTall #######################################

    # Adjust ratio main-pane/secondary-pane in MonadTall
    # and ratio of secondary panes 
    Key([mod], "y", lazy.layout.grow()),
    Key([mod, shift], "y", lazy.layout.shrink()),
    
    Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "m", lazy.layout.maximize()),
   
    # Floating #######################################

    Key([mod], "v", lazy.window.toggle_floating()),
    Key([mod, shift], "x", lazy.spawn("xscreensaver-command --lock"))]

hotkeys = [
    ("Return", "$TERMINAL"),
    ("e", "$EDITOR"),
    ("b", "chromium"),
    ("r", "dmenu_run -i -nb '#000000'"),
    ("c", "gmrun")]

for key, command in hotkeys:
    keys.append(Key([mod], key, lazy.spawn(command)))

layouts = [
    layout.MonadTall(),
    layout.Stack(stacks=2),
    layout.Max()]

groups = []
g_names = [
    'chat', 'term', 'edit', 'web',
    'web2', 'edit2', 'term2', 'docs']
g_keys = [
    'a', 's', 'd', 'f',
    'j', 'k', 'l', 'semicolon']
g_screens = [
    0, 0, 0, 0,
    1, 1, 1, 1]

widget_defaults = dict( 
	font='Consolas',
	fontsize = 16,
	padding = 3,
	margin_x = 1,
	margin_y = 1,
	borderwidth = 1)

if hostname == work_hostname:
    for name, key, screen in zip(g_names, g_keys, g_screens):
        groups.append(Group(name))
        keys.append(
            Key([mod], key, lazy.to_screen(screen),
                lazy.group[name].toscreen(screen)))
        keys.append(
            Key([mod, shift], key, lazy.window.togroup(name)))
    
    screens = [
        Screen(
            bottom = bar.Bar([
                widget.GroupBox(
                    borderwidth=2,
                    fontsize=14,
                    this_current_screen_border = "009900"),
                widget.Sep(),
                widget.CurrentLayout(),
                widget.Sep(),
                widget.WindowName(fontsize = 16),
                widget.Sep()],
                24)),
        Screen(
            bottom = bar.Bar([
                widget.GroupBox(
                    borderwidth=2,
                    fontsize=14,
                    this_current_screen_border = "009900"),
                widget.Sep(),
                widget.CurrentLayout(),
    	        widget.Sep(),
                widget.WindowName(fontsize=16),
                widget.Sep(),
                widget.Systray(),
    		    widget.Volume(),
                widget.Sep(),
                widget.Clock('%k:%M  %d %B')],
                24))]
elif hostname == home_hostname:
    for name, key in zip(g_names, g_keys):
        groups.append(Group(name))
        keys.append(
            Key([mod], key, lazy.to_screen(),
                lazy.group[name].toscreen()))
        keys.append(
            Key([mod, shift], key, lazy.window.togroup(name)))

    battery_defaults = dict(
        battery_name='BAT1',
        energy_now_file='charge_now',
        energy_full_file='charge_full',
        power_now_file='current_now')

    screens = [
        Screen(
            bottom = bar.Bar([
                widget.GroupBox(
                    borderwidth=2,
                    fontsize=14),
                widget.Sep(),
                widget.CurrentLayout(),
                widget.Sep(),
                widget.WindowName(fontsize = 16),
                widget.Sep(),
                widget.BatteryIcon(**battery_defaults),
                widget.Battery(
                    foreground = '70FF70',
                    low_foreground = 'FF0000',
                    update_delay = 5,
                    format = '{percent:2.0%} {hour:d}:{min:02d}',
                    low_percentage = 0.20,
                    hide_threshold = True,
                    **battery_defaults),
                widget.Wlan(),
                widget.Sep(),
                widget.Systray(),
    		    widget.Volume(),
                widget.Sep(),
                widget.Clock('%k:%M  %d %B')],
                24))]

mouse = [
        Drag([mod], "Button1", lazy.window.set_position_floating(),
            start=lazy.window.get_position()),
        Drag([mod], "Button3", lazy.window.set_size_floating(),
            start=lazy.window.get_size()),
        Click([mod], "Button2", lazy.window.bring_to_front())]

floating_layout = layout.Floating(auto_float_types=[
    "notification",
    "toolbar",
    "splash",
    "dialog"])

floating_names = [
    "gmrun",
    "pidgin",
    "skype",
    "vlc",
    "teamviewer.exe",
    "wine",
    "eclipse"]

@hook.subscribe.client_new
def floating_from_range(window):
    wm_class = window.window.get_wm_class()
    try:
        if wm_class[1].lower() in floating_names:
            window.floating = True
    except TypeError:
        pass

@hook.subscribe.client_new
def floating_dialogs(window):
    dialog = window.window.get_wm_type() == 'dialog'
    transient = window.window.get_wm_transient_for()
    if dialog or transient:
        window.floating = True

import subprocess
import re

def is_running(process):
    s = subprocess.Popen(["ps", "axuw"], stdout=subprocess.PIPE)
    for x in s.stdout:
        if re.search(process, x):
            return True
    return False

def execute_once(process):
    if not is_running(process):
        return subprocess.Popen(process.split())

# start the applications at Qtile startup
#@hook.subscribe.startup
#def startup():
    #execute_once("xmodmap /home/dedxoxo/.Xmodmap")
    #execute_once("xsetroot -cursor_name left_ptr")
    #execute_once("dropboxd")
    #execute_once("xxkb")
    #subprocess.Popen("feh --bg-tile /home/dedxoxo/Pictures/current".split())

