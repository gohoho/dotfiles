" запуск интерпретатора на F5
nnoremap <F5> :ConqueTermSplit ipython<CR>

" проверка кода в соответствии с PEP8 через <leader>8
map <buffer> <leader>8 :PymodeLint<CR>

"set completeopt-=preview " раскомментируйте, в случае, если не надо, чтобы jedi-vim показывал документацию по методу/классу
setlocal expandtab shiftwidth=4 tabstop=8
\ formatoptions+=croq softtabstop=4 smartindent
\ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
