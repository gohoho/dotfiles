setlocal autowrite
map! <F5> <Esc>:make<CR>
map <F5> :make<CR>
" run make clean
map! <S-F5> <Esc>:make clean<CR>
map <S-F5> :make clean<Cr>
" run make run
map! <C-S-F5> <Esc>:make run<CR>
map <C-S-F5> :make run<CR>
" go to previous error
map! <F6> <Esc>:cp<Cr>zvzz:cc<Cr>
map <F6> :cp<Cr>zvzz:cc<Cr>
" show cwindow on F7
map! <F7> <Esc>:cw 6<CR>
map <F7> :cw 6<CR>
" hide cwindow on Shift-F7
map! <S-F7> <Esc>:ccl<CR>
map <S-F7> :ccl<CR>
" go to next error
map! <F8> <Esc>:cn<Cr>zvzz:cc<Cr>
map <F8> :cn<Cr>zvzz:cc<Cr>
