#
# ~/.bash_profile
#

#jre
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'

[[ -f ~/.bashrc ]] && . ~/.bashrc
